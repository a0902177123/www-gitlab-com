---
layout: handbook-page-toc
title: Monthly Announcements
description: "The Learning & Development team produces a monthly announcement each month. This page outlines the process and results of that initiative."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Learning & Development Monthly Announcements

In FY22 Q1 and Q2, the L&D team noticed low attendance and participation in the team's group conversations. These monthly announcements are an interation on the group conversations. The goals of these announcements include:

1. Announce upcoming L&D programs for team members
1. Field async questions about L&D and from the team
1. Increase participation from team members in all time zones

### Monthly Announcement Strucutre

Monthly Announcements from the L&D team are a 3-part announcement. This includes:

1. Async AMA issue for sharing upcoming opportunities, key results, and threaded discussion
2. Short, 2 minute video recorded using Loom and shared via Slack

Future iterations of the announcement will include:

1. Using Bananatag to share the content via email with the team and Email that highlights opportunities and results
1. Leveraging relationships with managers and PBPs to share in division specific channels

### Preparation checklist for GitLab team members

1. Record short video using Loom that highlights 2-3 opportunities coming up and 1-2 key results from the previous month
1. Open and populate an issue using the `monthly-announcement` template. Collaborate with L&D team to include all important events in this issue body.
1. Post video, link to slides, and link to AMA issue in Slack on the first Tuesday of the month in the #whats-happening-at-gitlab Slack channel
1. Share the post from the #whats-happening-at-gitlab channel to 
     - the #managers channel and ask managers to share with their teams
     - the PBPs and ask to amplify with division leadership
1. Add a link to the issue and video to the onboarding issue template

### Schedule

Monthly announcements are shared by the L&D team on the first Tuesday of each month. Below, you'll find links to previous announcement materials.

| Month | Video Link | Async AMA | Total Views | Total Comments |
| ----- | ----- | ----- | ----- | ----- |
| 2021-09 | [Video](https://www.loom.com/share/a087f45f78134f04a8260fd181cead53) | [Issue](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/305)| 44 | 1 |
| 2021-10 | [Video](https://www.loom.com/share/eee099e204a54769a050babee0b67c6c) | [Issue](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/324) | 36 | 0 |
| 2021-11 | [Video](https://www.loom.com/share/a30aecc6bec8449cb391b9f9fb4a4775) | [Issue](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/338) | 59 | 1 |
| 2021-12 | [Video](https://www.loom.com/share/6d6d103ec30942fda2bd48114aaab8a5) | [Issue](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/348) | 51 | 0 | 
| 2022-01 | [Video](https://www.loom.com/share/74ded140aff64ee2a28ebff31f066262) | [Issue](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/359) | 51 | 0 |
| 2022-02 | [Video](https://www.loom.com/share/e7d975e055f54d8fb30c47ddec6d130e) | [Issue](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/372) | 70 | 0 |
| 2022-03 | [Video](https://www.loom.com/share/61ff5f3320a945bb98639fbb95b8003c) | [Issue](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/383) | 22 | 0 |
| 2022-04 | [Video](https://www.loom.com/share/3a52e3dd4ff54d9abfc1f90baafee5c9) | [Issue](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/395) | 22 | ----- |
| 2022-05 | [Video](https://www.loom.com/share/8db8338f8cd94b06bfd592afeb28c5f3) | [Issue](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/400) | ----- | ----- |
| 2022-06 | ----- | ----- | ----- | ----- |
| 2022-07 | ----- | ----- | ----- | ----- |



### Results

The following metrics will be used to measure results of the L&D monthly announcements

| Metric | Monthly Goal |
| ----- | ----- | 
| Total Loom Video Views | 20% of company | 
| Total comments/questions posted on the issue | 10 | 
